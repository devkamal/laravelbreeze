<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Models\Admin;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function Index(){
        return view('admin.login');
    }

    public function Login(Request $request){
        $check = $request->all();
        if(Auth::guard('admin')->attempt( ['email' => $check['email'], 'password' => $check['password'] ] ) ){
            return redirect()->route('admin_dashboard')->with('error','Admin login success');
        }else{
            return back()->with('error','Invalid Email or Password');
        }
    }

    public function Dashboard(){
        return view('admin.index');
    }

    public function adminLogout(){
        Auth::guard('admin')->logout();
        return redirect()->route('login_form')->with('success','Logout Success');
    }

    // Start of admin register portion
 
    public function adminRegister(){
        return view('admin.register.register_form');
    }

    public function adminStore(Request $request){
        $validateData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);

        Admin::insert([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'created_at' => Carbon::now(),
        ]);
        return redirect()->route('login_form')->with('success','Admin registered success');

    }


}
