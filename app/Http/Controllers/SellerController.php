<?php

namespace App\Http\Controllers;

use App\Models\Seller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use Carbon\Carbon;

class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Index()
    {
        return view('seller.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Login(Request $request)
    {
        // dd($request->all());

        $check = $request->all();
        if(Auth::guard('seller')->attempt( ['email' => $check['email'], 'password' => $check['password'] ] ) ){
            return redirect()->route('seller_dashboard')->with('success','Seller login success');
        }else{
            return back()->with('error','Invalid Email or Password');
        }

    }

    public function Dashboard(){
        return view('seller.index');
    }


    public function sellerLogout()
    {
        Auth::guard('seller')->logout();
        return redirect()->route('seller_login_form')->with('success','Seller logout success');
    }

    //Start of Seller registration portion
   public function sellerRegister(){
        return view('seller.register.register_form');
   }

   public function sellerStore(Request $request){
        $validateData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);

        Seller::insert([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'created_at' => Carbon::now(),
        ]);
        return redirect()->route('seller_login_form')->with('success','Seller registered success');
   }


}
