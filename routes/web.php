<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\SellerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

//******==================Start of Admin Controller***********/
Route::prefix('admin')->group(function(){
    Route::get('/login', [AdminController::class, 'Index'])->name('login_form');
    Route::post('/login/owner', [AdminController::class, 'Login'])->name('admin_login');
    Route::get('/dashboard', [AdminController::class, 'Dashboard'])->name('admin_dashboard')->middleware('admin');
    Route::get('/logout', [AdminController::class, 'adminLogout'])->name('admin_logout');

    Route::get('/register', [AdminController::class, 'adminRegister'])->name('admin_register');
    Route::post('/register/store', [AdminController::class, 'adminStore'])->name('admin_register_store');
});
//******==================End of Admin Controller***********/

//******==================Start of Seller Controller***********/
Route::prefix('seller')->group(function(){
    Route::get('/login', [SellerController::class, 'Index'])->name('seller_login_form');
    Route::post('/login/owner', [SellerController::class, 'Login'])->name('seller_login');
    Route::get('/dashboard', [SellerController::class, 'Dashboard'])->name('seller_dashboard')->middleware('seller');
    Route::get('/logout', [SellerController::class, 'sellerLogout'])->name('seller_logout');

    Route::get('/register', [SellerController::class, 'sellerRegister'])->name('seller_register');
    Route::post('/register/store', [SellerController::class, 'sellerStore'])->name('seller_register_store');
});
//******==================End of Seller Controller***********/
