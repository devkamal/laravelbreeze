@extends('seller.master')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">

        @if (Session::has('success'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong> {{ session::get('success')}}</strong>
          </div>
        @endif
        
            <h1 class="title">Hey, I'm Seller Dashboard!</h1>
            Seller login name: {{ Auth::guard('seller')->user()->name }} <br>
            Seller Email Id: {{ Auth::guard('seller')->user()->email }} <br>

            <a href="{{ route('seller_logout') }}" class="dashboard-nav-item"><i class="fas fa-sign-out-alt"></i> Logout </a>

        </div>
    </div>
</div>

@endsection